
from Tkinter import *

class board(Frame):
    def __init__(self, parent, shapes):
        # communicates with game class
        self.parent = parent # this should be tkinter window (root)
        self.canvas = Canvas(parent, height = 300, width=600, background="black")
        self.canvas.pack()
        self.images = {} 
        self.shapes = shapes
        self.initUI()

    def initUI(self):
        # makes dictionary of images of each shape
        for key, value in self.shapes.items():
            positions = self.offset(key, value)
            pic = self.draw_shape(key, value, positions)
            self.images[key] = pic
        
    def draw_shape(self, name, shape, positions):
        # used by other methods to draw individual shape
        # uses positions of box around shape to draw shape
        # tkinter create_line method draws lines that outline shape
        # circle is drawn as rectangle with rounded corners.
        # keyword smooth rounds rectangle corners.
        is_smooth = 0
        color = "white"
        image = self.canvas.create_line(*positions, 
            smooth = is_smooth, fill=color)
        return image

    def offset(self, name, shape):
        if name[0:6] =="paddle":
            offset = [5, 5, -5, 5, -5, -5, 5, -5, 5, 5]
            positions = [shape[i]+offset[i] for i in range(10)]
        elif name[0:6] == "border":
            offset = [-5, -5, 5, -5, 5, 5, -5, 5, -5, -5]
            positions = [shape[i]+offset[i] for i in range(10)]
        else:
            positions = shape
        return positions

    def update_UI(self):
        # updates images dictionary based on shapes dictionary
        for name in self.images:
            positions = self.offset(name, self.shapes[name])
            self.canvas.coords(self.images[name], *positions)

class controls(Frame):
    ''' This class takes user input from keys that control the paddles and it
    makes start/pause buttons and displays score. It communicates with model.'''
    def __init__(self, parent, shapes):
        self.parent = parent
        self.shapes = shapes
        self.running = True # changes when user presses start or pause buttons
        # Bind keys so players can move paddle
        self.buttons = set()
        self.parent.bind_all("<KeyPress>", lambda e: self.buttons.add(e.keysym))
        self.parent.bind_all("<KeyRelease>", lambda e: self.buttons.discard(e.keysym))
        self.paddle_velocity = [ " 0 0", " 0 0"]

    def set_paddle(self, paddle, button, velocity):
        # called by get_input method
        # updates paddle based on what buttons are pressed
        # paddle argument is a key in the dictionary of game pieces
        # button = [up button, down button]

        y = self.shapes[paddle][1]

        #if paddle == "paddleA":
        #    index = 0
        #elif paddle == "paddleB":
        #    index = 1

        index = 0

        if button[0] in self.buttons and y>0:
            self.paddle_velocity[index] = " 0-2"
        elif button[1] in self.buttons and y<300:
            self.paddle_velocity[index] = " 0 2"
        else:
            self.paddle_velocity[index] = " 0 0"

    def get_input(self):
        # maps paddles, buttons and paddle velocities to set_paddle
        paddles = ["paddleA", "paddleB"] # keys of paddles in game piece dictionary
        buttons = [["e", "s"], ["Up", "Down"]]
        velocities = [2] * 2 # all velocities are 5 in the y direction
        # velocity sign is changed depending on which button is pressed.
        map(self.set_paddle, paddles, buttons, velocities)