# pong client
'''
Instructions: run this code in a separate consol after starting pong_server.py 
			  then type "take turn" into console to update game 
			  '''

import socket

import Tkinter
from simple_pong_view import *

class top_level(Frame):
	'''This class makes instances of game, board and controls (model, view,
	 controller). It also defines a tkinter frame shared by board and controls.
	 The run method recursively updates everything.'''
	def __init__(self, parent, shapes):
		Frame.__init__(self, parent)
		self.parent = parent
		self.field = board(self.parent, shapes)
		self.director = controls(self.parent, shapes)
		self.pack()

	def update(self, shapes):
		self.field.shapes.update(shapes)
		self.field.update_UI()
		self.director.shapes.update(shapes)
		self.director.set_paddle("paddleB", ["Up", "Down"], 2)
		self.parent.update()

class mysocket:

	socket_family = socket.AF_INET
	socket_type = socket.SOCK_STREAM
	host = 'localhost'
	port = 50000
	buffer_size=1024

	def __init__(self):

		self.sckt = socket.socket(mysocket.socket_family, mysocket.socket_type)
		self.sckt.connect((mysocket.host, mysocket.port))
		print "TCP client created....\n"
		print "connection to server @ " + mysocket.host + ":" + str(mysocket.port)

	def start_game(self):
		self.sckt.send(" 0 0")
		data = self.sckt.recv(mysocket.buffer_size)
		shapes = self.convert_string_to_dict(data)
		return shapes

	def convert_string_to_dict(self, data):
		shapes = {}
		first = data.split( ']')
		for item in first[0:-1]:
			second = item.split('[')
			name = second[0]
			numbers = second[1].split('.0')
			numbers = numbers[0:-1]
			for i in range(len(numbers)):
				if numbers[i][0] == ',':
					numbers[i] = int(numbers[i][2:])
				else:
					numbers[i] = int(numbers[i])
			shapes[name] = numbers
		return shapes

root = Tkinter.Tk()
mysckt = mysocket()
parts = mysckt.start_game()
window = top_level(root, parts)
message = " 0 0"
terminate = False
while not terminate:
	mysckt.sckt.send(message)
	data = mysckt.sckt.recv(mysocket.buffer_size)
	parts = mysckt.convert_string_to_dict(data)
	window.update(parts)
	print "recieved "
	message = window.director.paddle_velocity[0]
	if message == 'q': terminate = True

mysckt.sckt.close()
print "...Done!"

