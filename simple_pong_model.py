
import numpy as np 
from numpy.linalg import norm
import random


class piece:
	# This class represents individual game pieces.
	# The game class creates instances of piece and represents the overall game.

	def __init__(self, coords, dimensions, velocity, moving = False):
		# for coords and velocity we keep a copy of the initial values to reset
		self.init_coords = np.array(coords, dtype=float) 
		self.coords = self.init_coords[:]# Center of shape
		self.init_velocity = np.array(velocity, dtype=float)
		self.velocity = self.init_velocity[:]

		self.dimensions = np.array(dimensions, dtype=float) # [width/2, height/2]
		self.get_positions()
		self.moving = moving 

	def get_positions(self):
		# Gets four corners of box around shape based on centerpoint and dimensions.
		x,y = self.coords
		w,h = self.dimensions
		self.positions = [x-w, y-h, x+w, y-h, x+w, y+h, x-w, y+h, x-w, y-h]

	def move(self, reset=0):
		# called later to update each game piece
		if reset == 1:
			self.coords = self.init_coords[:]
			self.velocity = self.init_velocity[:]

		self.coords = np.array(self.coords, dtype = float)
		self.velocity = np.array(self.velocity, dtype = float)
		self.coords += self.velocity
		self.get_positions()

	def check_inscreen(self, bounds):
		x,y = self.coords
		left, top, right, bottom = bounds

		if y <= top or y >= bottom:
			return False
		elif x <= left or x >= right:
			return False
		else:
			return True


class rectangle(piece):
	# This class adds rectangle specific stuff to piece class
	def __init__(self, coords, dimensions, velocity, moving=False):
		self.moving = moving
		self.piece = piece.__init__(self, coords, dimensions, velocity, moving=moving)
		self.lines = []
		self.get_lines() 

	def get_lines(self):
		# Gets lines of rectangles sides. 
		# [x1, y1, x2, y2] This is how each line is represented.
		# Having the coordinates of these lines is useful in collision detection.
		self.lines = []
		for i in range(4,len(self.positions)+1, 2):
			self.lines.append(self.positions[i-4:i])

	def move(self):
		# Uses move method of parent class.
		# Then updates lines since lines are specific to rectangle. 
		piece.move(self)
		for line in self.lines:
			line[0:2] += self.velocity
			line[2:4] += self.velocity

class circle(piece):
	# This class adds circle specific stuff to piece class.
	def __init__(self, coords, dimensions, velocity, moving=False):
		self.moving=moving
		piece.__init__(self, coords, dimensions, velocity, moving=moving)
		# ball radius is dimensions
		# dimensios = [width/2, height/2]

	def check_collision(self, shape):
		# line argument is list of coordinates that represent a line
		# line = [x1, y1, x2, y2]
		hit = 0
		for line in shape.lines:
			# Find vectors from ball to line endpoints.
			p1 = np.array(line[0:2], dtype=float) # line endpoints
			p2 = np.array(line[2:4], dtype=float)
			v1 = p1 - self.coords #Vectors from ball to line end points
			v2 = p2 - self.coords 

			# Find unit vector orthogonal to line
			x, y = p2 - p1 # vector between line endpoints
			line_norm = np.sqrt(x**2 + y**2) # norm of line
			line_orth = np.array([y, -x])/line_norm # orthogonal unit vector
			ray = self.velocity - shape.velocity

			# Find distance of ball to line and...
			# ...distance ball will move towards line in the next time step
			proj_distance = np.dot(v1, line_orth) #project ball to endpoint on unit normal of line 
			proj_velocity = np.dot(ray, line_orth)# project velocity on unit normal

			# Vector projections used to see if ball path is within endpoints of line
			proj_v1_v2 = np.dot(v1, v2) 
			proj_v1_velocity = np.dot(v1, ray)
			proj_v2_v1 = np.dot(v2,v1)
			proj_v2_velocity = np.dot(v2, ray)

			# See if velocity vector is between vectors from ball to line ends.
			within_edges = False 
			if proj_v1_v2 < proj_v1_velocity and proj_v2_v1 < proj_v2_velocity:
				within_edges = True

			# -If velocity projection is longer than distance projection,
			#  the ball will move past the line within the next time step.
			# -If ball path is also within the ends of the line,
			#  the ball will cross the line.
			if abs(proj_velocity) > abs(proj_distance) and within_edges:
				new_direction = np.dot(ray, line_orth)*line_orth 
				self.velocity = self.velocity - 2*new_direction # velocity vector after reflection
				hit = True
		return hit

class game:
	''' The purpose of this class is to assemble all instances of game pieces
	and update them altogether.'''
	def __init__(self):
		self.ball = circle([430, 100], [5, 5], [-1,1], moving = True)
		paddleA = rectangle([50, 150], [10, 25], [0, 0], moving = True)
		paddleB = rectangle([550, 250], [10, 25], [0,0], moving=True)
		border = rectangle([300, 150], [300, 150], [0, 0])

		keys = ["zball", "paddleA", "paddleB", "border"]
		values = [self.ball, paddleA, paddleB, border]
		self.parts = dict(zip(keys, values))
		
		keys = ["paddleA", "paddleB", "border"]
		values = [paddleA, paddleB, border]
		self.walls = dict(zip(keys, values))

		keys = ["paddleA", "paddleB", "zball"]
		values = [paddleA, paddleB, self.ball]
		self.moving_parts = dict(zip(keys, values))

	def reset_pieces(self):
		reset = False

		if self.parts["zball"].coords[0] < 10:
			reset = True
		elif self.parts["zball"].coords[0] > 590:
			reset = True

		if reset == True:
			self.parts["zball"].move(reset=1)

	def update_pieces(self):
		''' This method loops through the game pieces to call the "move" method
		for each one. Then it loops through them again to check for collision. 
		Sometimes there is more than one line that ball might collide with in the 
		next time step. For this, we identify which line is closest to the ball.
		This method is kind of messy. Maybe it should be reorganized.'''

		# collision detection loop
		for name, part in self.walls.items():
			collision = self.parts["zball"].check_collision(part)

		for part in self.moving_parts.values():
			if part.moving == True:
				part.move() # update positions of each game piece.