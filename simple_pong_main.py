
import Tkinter
from simple_pong_view import *
from simple_pong_model import *
from time import sleep
import sys

class top_level(Frame):
	'''This class makes instances of game, board and controls (model, view,
	 controller). It also defines a tkinter frame shared by board and controls.
	 The run method recursively updates everything.'''
	def __init__(self, parent, game, shapes):
		Frame.__init__(self, parent)
		self.parent = parent
		self.game = game
		self.director = controls(self, self.game)
		self.field = board(self.parent, shapes)
		self.pack()
		parent.update()

	def update(self, shapes):
		self.field.shapes.update(shapes)
		if self.director.running: # check if user has pushed start button
			self.field.update_UI() # update view
			self.director.get_input() # get user input
			self.parent.update()

def run():
	match.update_pieces()
	data = convert_dict_to_string(match.parts)
	shapes = contert_string_to_dict(data)
	window1.update(shapes)
	window2.update(shapes)
	sleep(0.001)
	#root1.update()

def convert_dict_to_string(shapes):
	data = ""
	for key, value in shapes.items():
		data = data + str(key) + str(value.positions)
	return data

def contert_string_to_dict(data):
	shapes = {}
	first = data.split( ']')
	for item in first[0:-1]:
		second = item.split('[')
		name = second[0]
		numbers = second[1].split('.0')
		numbers = numbers[0:-1]
		for i in range(len(numbers)):
			if numbers[i][0] == ',':
				numbers[i] = int(numbers[i][2:])
			else:
				numbers[i] = int(numbers[i])
		shapes[name] = numbers
	return shapes

root1 = Tkinter.Tk()
root2 = Tkinter.Tk()
match = game()
data = convert_dict_to_string(match.parts)
shapes = contert_string_to_dict(data)
window1 = top_level(root1, match, shapes)
window2 = top_level(root2, match, shapes)
while root1:
	run()
root1.destroy()
root2.destroy()
