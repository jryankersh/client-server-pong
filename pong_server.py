#pong server
import socket, sys
from simple_pong_model import *
from time import sleep
'''
Instructions: run this code in a console window
			  then run pong_client1.py in another console
			  then update the game by typing "take turn" in the pong_client1 console
			  '''


def convert_dict_to_string(shapes):
	data = ""
	for key, value in shapes.items():
		data = data + str(key) + str(value.positions)
	return data

print "creating TCP socket..."

host = 'localhost'
port1 = 50000
port2 = 50001
buffer_size = 1024
backlog = 5
socket_family = socket.AF_INET
socket_type = socket.SOCK_STREAM
socket_options = (socket.SOL_SOCKET, socket.SO_REUSEADDR, buffer_size)

def create_socket(port):
	try:
		sckt = socket.socket(socket_family, socket_type)
		print sckt._sock
		sckt.bind((host, port))
		print "TCP server started..."
		print "host: ", host, "port: ", port
		sckt.listen(backlog)
	except socket.error, (code, message):
		if sckt: sckt.close()
		print "socket error: ", code
		print "Could not open socket: ", message
		sys.exit(1)
	return sckt

def establish_connection(sckt):
	client_connection, client_address = sckt.accept()
	print "connection established with..."
	print "IP address: " +str(client_address) + \
			", connection: " + str(client_connection)
	return [client_connection, client_address]


sckt1 = create_socket(port1)
sckt2 = create_socket(port2)

match = game()
shapes = convert_dict_to_string(match.parts)

terminate = False
while not terminate:

	client_connection1, client_address1 = establish_connection(sckt1)
	client_connection2, client_address2 = establish_connection(sckt2)

	while True:
		data1 = client_connection1.recv(buffer_size)
		data2 = client_connection2.recv(buffer_size)

		print data1, data2

		match.parts["paddleA"].velocity = [int(data2[0:2]), int(data2[2:4])]
		match.parts["paddleB"].velocity = [int(data1[0:2]), int(data1[2:4])]
		match.update_pieces()
		shapes = convert_dict_to_string(match.parts)
		client_connection1.send(shapes)
		client_connection2.send(shapes)
		print "recieved data"
		message = str(data1).upper()
		sleep(0.01)


		sckt1.close()
		sckt2.close()
		if message == 'Q': terminate = True
	if sckt1: sckt1.close()
	if sckt2: sckt2.close()
	print "TCP server closing...Done!"